import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TeamCreationFormComponent } from "./team-creation-form.component";
import { MatInputModule, MatIconModule } from "@angular/material";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe("TeamCreationFormComponent", () => {
  let component: TeamCreationFormComponent;
  let fixture: ComponentFixture<TeamCreationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamCreationFormComponent],
      imports: [
        MatInputModule,
        MatIconModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamCreationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
