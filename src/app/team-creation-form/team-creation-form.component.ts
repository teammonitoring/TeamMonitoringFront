import { Component, OnInit } from "@angular/core";
import { TeamService } from "../team.service";
import { EventBusService } from "../event-bus.service";
import { Team } from "../team";
import { Message } from "../message";

@Component({
  selector: "app-team-creation-form",
  templateUrl: "./team-creation-form.component.html",
  styleUrls: ["./team-creation-form.component.css"]
})
export class TeamCreationFormComponent implements OnInit {
  constructor(
    private teamService: TeamService,
    private eventBusService: EventBusService
  ) {}

  ngOnInit() {}

  addTeam(name: string) {
    this.teamService.createTeam({ name } as Team).subscribe(team => {
      this.eventBusService.publish({
        type: "TEAM_CREATED",
        data: team.id
      } as Message);
    });
  }
}
