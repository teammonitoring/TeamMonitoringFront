import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { HttpProbeOverviewComponent } from "./http-probe-overview.component";
import { HttpClientModule } from "@angular/common/http";
import { MatIconModule } from "@angular/material";

describe("HttpProbeOverviewComponent", () => {
  let component: HttpProbeOverviewComponent;
  let fixture: ComponentFixture<HttpProbeOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HttpProbeOverviewComponent],
      imports: [HttpClientModule, MatIconModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpProbeOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
