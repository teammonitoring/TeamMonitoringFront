import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Probe } from "../probe";
import { HttpProbeService } from "../http-probe.service";
import { EventBusService } from "../event-bus.service";
import { timer, Subscription } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-http-probe-overview",
  templateUrl: "./http-probe-overview.component.html",
  styleUrls: ["./http-probe-overview.component.css"]
})
export class HttpProbeOverviewComponent implements OnInit, OnDestroy {
  @Input() probe: Probe;
  @Input() canDelete: boolean;
  state: string = "unknown";
  timer: Subscription;

  constructor(
    private httpProbeService: HttpProbeService,
    private eventBusService: EventBusService,
    private httpClient: HttpClient
  ) {}

  ngOnInit() {
    this.timer = timer(0, 30 * 1000).subscribe(() => this.callUrl());
  }

  ngOnDestroy(): void {
    this.timer.unsubscribe();
  }

  callUrl() {
    this.state = "unknown";
    this.httpClient.get(this.probe.url).subscribe(
      () => {
        this.state = "success";
      },
      () => {
        this.state = "failure";
      }
    );
  }

  delete() {
    this.httpProbeService.deleteProbe(this.probe.id).subscribe(() => {
      this.eventBusService.publish({
        type: "TEAM_UPDATED",
        data: this.probe.teamId
      });
    });
  }
}
