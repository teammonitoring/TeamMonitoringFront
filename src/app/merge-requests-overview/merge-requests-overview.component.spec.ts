import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MergeRequestsOverviewComponent } from "./merge-requests-overview.component";
import { HttpClientModule } from "@angular/common/http";

describe("MergeRequestsOverviewComponent", () => {
  let component: MergeRequestsOverviewComponent;
  let fixture: ComponentFixture<MergeRequestsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MergeRequestsOverviewComponent],
      imports: [HttpClientModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeRequestsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
