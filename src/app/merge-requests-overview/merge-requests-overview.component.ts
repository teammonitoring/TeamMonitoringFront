import { Component, OnInit, Input } from "@angular/core";
import { TeamService } from "../team.service";
import { EventBusService } from "../event-bus.service";
import { MergeRequest } from "../merge-request";

@Component({
  selector: "app-merge-requests-overview",
  templateUrl: "./merge-requests-overview.component.html",
  styleUrls: ["./merge-requests-overview.component.css"]
})
export class MergeRequestsOverviewComponent implements OnInit {
  @Input() teamId: string;
  mergeRequests: MergeRequest[];

  constructor(
    private teamService: TeamService,
    private eventBusService: EventBusService
  ) {}

  ngOnInit() {
    this.eventBusService.messages.subscribe(message => {
      if (
        message.type === "CONNECTOR_CREATED" &&
        message.data === this.teamId
      ) {
        this.getMergeRequests();
      }
    });
    this.getMergeRequests();
  }

  getMergeRequests() {
    this.teamService
      .getMergeRequests(this.teamId)
      .subscribe(mergeRequests => (this.mergeRequests = mergeRequests));
  }

  getMergeRequestBgClass(mergeRequest: MergeRequest) {
    const date = new Date(mergeRequest.createdAt);
    const dateDiff = +new Date() - +date;
    return dateDiff > 1000 * 60 * 60 * 24 * 7
      ? "bg-danger"
      : dateDiff > 1000 * 60 * 60 * 24
      ? "bg-warning"
      : "bg-light";
  }

  getOverviewBg() {
    return this.mergeRequests.length < 3 ? "bg-success" : "";
  }
}
