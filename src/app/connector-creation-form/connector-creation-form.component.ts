import { Component, OnInit, Input } from "@angular/core";
import { ConnectorService } from "../connector.service";
import { EventBusService } from "../event-bus.service";
import { Connector } from "../connector";
import { Message } from "../message";

@Component({
  selector: "app-connector-creation-form",
  templateUrl: "./connector-creation-form.component.html",
  styleUrls: ["./connector-creation-form.component.css"]
})
export class ConnectorCreationFormComponent implements OnInit {
  @Input() teamId: string;
  type: string;
  url: string;
  privateToken: string;
  teamReference: string;
  availableTypes: string[];

  constructor(
    private connectorService: ConnectorService,
    private eventBus: EventBusService
  ) {}

  ngOnInit() {
    this.connectorService.getAvailableType().subscribe(types => {
      this.availableTypes = types;
    });
  }

  create() {
    this.connectorService
      .createConnector({
        type: this.type,
        url: this.url,
        privateToken: this.privateToken,
        teamId: this.teamId,
        teamReference: this.teamReference
      } as Connector)
      .subscribe(() => {
        this.eventBus.publish({
          type: "CONNECTOR_CREATED",
          data: this.teamId
        } as Message);
        this.reset();
      });
  }

  reset() {
    this.type = "";
    this.url = "";
    this.privateToken = "";
    this.teamReference = "";
  }
}
