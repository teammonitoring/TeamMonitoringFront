import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ConnectorCreationFormComponent } from "./connector-creation-form.component";
import { MatInputModule, MatSelectModule } from "@angular/material";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe("ConnectorCreationFormComponent", () => {
  let component: ConnectorCreationFormComponent;
  let fixture: ComponentFixture<ConnectorCreationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectorCreationFormComponent],
      imports: [
        MatInputModule,
        MatSelectModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectorCreationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
