import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Connector } from "./connector";

@Injectable({
  providedIn: "root"
})
export class ConnectorService {
  private connectorsUrl: string = "/v1/connectors";

  constructor(private httpClient: HttpClient) {}

  getProbes(): Observable<Connector[]> {
    return this.httpClient.get<Connector[]>(this.connectorsUrl);
  }

  getConnectorsByTeamId(teamId: string): Observable<Connector[]> {
    const url = `${this.connectorsUrl}?teamId=${teamId}`;
    return this.httpClient.get<Connector[]>(url);
  }

  getConnector(id: string): Observable<Connector> {
    const url = `${this.connectorsUrl}/${id}`;
    return this.httpClient.get<Connector>(url);
  }

  createConnector(connector: Connector): Observable<Connector> {
    return this.httpClient.post<Connector>(this.connectorsUrl, connector);
  }

  updateConnector(connector: Connector): Observable<Connector> {
    const url = `${this.connectorsUrl}/${connector.id}`;
    return this.httpClient.put<Connector>(url, connector);
  }

  deleteConnector(id: string): Observable<any> {
    const url = `${this.connectorsUrl}/${id}`;
    return this.httpClient.delete<any>(url);
  }

  getAvailableType(): Observable<string[]> {
    return this.httpClient.get<string[]>(`${this.connectorsUrl}/types`);
  }
}
