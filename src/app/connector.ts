export class Connector {
  id: string;
  type: string;
  url: string;
  privateToken: string;
  teamId: string;
  teamReference: string;
}
