import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Message } from "./message";

@Injectable({
  providedIn: "root"
})
export class EventBusService {
  // TODO: Find what's already in angular
  messages: Subject<Message> = new Subject<Message>();
  constructor() {}

  publish(message: Message) {
    this.messages.next(message);
  }
}
