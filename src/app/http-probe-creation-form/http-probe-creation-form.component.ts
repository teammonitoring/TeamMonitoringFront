import { Component, OnInit, Input } from "@angular/core";
import { Probe } from "../probe";
import { HttpProbeService } from "../http-probe.service";
import { EventBusService } from "../event-bus.service";

@Component({
  selector: "app-http-probe-creation-form",
  templateUrl: "./http-probe-creation-form.component.html",
  styleUrls: ["./http-probe-creation-form.component.css"]
})
export class HttpProbeCreationFormComponent implements OnInit {
  @Input() teamId: string;
  name: string;
  url: string;
  tag: string;

  constructor(
    private httpProbeService: HttpProbeService,
    private eventBusService: EventBusService
  ) {}

  ngOnInit() {}

  create(): void {
    this.httpProbeService
      .createProbe({
        name: this.name,
        teamId: this.teamId,
        url: this.url,
        tag: this.tag
      } as Probe)
      .subscribe(probe => {
        this.eventBusService.publish({
          type: "TEAM_UPDATED",
          data: probe.teamId
        });
        this.reset();
      });
  }

  reset(): void {
    this.name = "";
    this.url = "";
    this.tag = "";
  }
}
