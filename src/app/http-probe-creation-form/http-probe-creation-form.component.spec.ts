import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { HttpProbeCreationFormComponent } from "./http-probe-creation-form.component";
import { HttpProbeService } from "../http-probe.service";
import { EventBusService } from "../event-bus.service";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { Probe } from "../probe";
import { of } from "rxjs";
import { MatInputModule } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe("HttpProbeCreationFormComponent", () => {
  let component: HttpProbeCreationFormComponent;
  let httpProbeService: HttpProbeService;
  let eventBusService: EventBusService;
  let fixture: ComponentFixture<HttpProbeCreationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HttpProbeCreationFormComponent],
      providers: [
        HttpProbeCreationFormComponent,
        HttpProbeService,
        EventBusService
      ],
      imports: [
        FormsModule,
        MatInputModule,
        BrowserAnimationsModule,
        HttpClientModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpProbeCreationFormComponent);
    component = fixture.componentInstance;

    eventBusService = TestBed.get(EventBusService);
    httpProbeService = TestBed.get(HttpProbeService);

    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  describe("reset", () => {
    it("should empty name and url but keep team", () => {
      component.name = "some name";
      component.teamId = "some id";
      component.url = "some url";
      component.tag = "some tag";

      component.reset();

      expect(component.name).toBe("");
      expect(component.url).toBe("");
      expect(component.tag).toBe("");
      expect(component.teamId).toBe("some id");
    });
  });

  describe("create", () => {
    beforeEach(() => {
      component.teamId = "AwesomeId";
      component.url = "AwesomeUrl";
      component.name = "AwesomeName";
      component.tag = "AwesomeTag";
    });
    it("Should call save on probeservice and then emit event", () => {
      spyOn(httpProbeService, "createProbe").and.returnValue(
        of({ teamId: "AwesomeId" } as Probe)
      );

      eventBusService.messages.subscribe(obj => {
        expect(obj.type).toBe("TEAM_UPDATED");
        expect(obj.data).toBe("AwesomeId");
      });

      component.create();

      expect(httpProbeService.createProbe).toHaveBeenCalledWith({
        teamId: "AwesomeId",
        name: "AwesomeName",
        url: "AwesomeUrl",
        tag: "AwesomeTag"
      });
    });
  });
});
