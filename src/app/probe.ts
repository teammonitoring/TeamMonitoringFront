export class Probe {
  id: string;
  name: string;
  url: string;
  teamId: string;
  tag: string;
}
