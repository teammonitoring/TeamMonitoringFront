import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TeamsComponent } from "./teams.component";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "../app-routing.module";
import { TeamOverviewComponent } from "../team-overview/team-overview.component";
import { TeamEditionComponent } from "../team-edition/team-edition.component";
import { HttpProbeOverviewComponent } from "../http-probe-overview/http-probe-overview.component";
import { FormsModule } from "@angular/forms";
import { HttpProbeCreationFormComponent } from "../http-probe-creation-form/http-probe-creation-form.component";
import { AppModule } from "../app.module";

describe("TeamsComponent", () => {
  let component: TeamsComponent;
  let fixture: ComponentFixture<TeamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, AppRoutingModule, FormsModule, AppModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
