import { Component, OnInit } from "@angular/core";
import { Team } from "../team";
import { TeamService } from "../team.service";
import { Router } from "@angular/router";
import { EventBusService } from "../event-bus.service";

@Component({
  selector: "app-teams",
  templateUrl: "./teams.component.html",
  styleUrls: ["./teams.component.css"]
})
export class TeamsComponent implements OnInit {
  teams: Team[];
  breakpoint: number;

  constructor(
    private teamService: TeamService,
    private eventBusService: EventBusService
  ) {}

  ngOnInit() {
    this.computeBreakPoint(window.innerWidth);
    this.getTeams();
    this.eventBusService.messages.subscribe(message => {
      if (message.type === "TEAM_CREATED" || message.type === "TEAM_DELETED")
        this.getTeams();
    });
  }

  getTeams(): void {
    this.teamService.getTeams().subscribe(teams => {
      this.teams = teams;
    });
  }

  onResize(event) {
    this.computeBreakPoint(event.target.innerWidth);
  }

  computeBreakPoint(width: number) {
    this.breakpoint = Math.floor(width / 300) || 1;
  }
}
