import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ProjectsOverviewComponent } from "./projects-overview.component";
import { HttpClientModule } from "@angular/common/http";

describe("ProjectsOverviewComponent", () => {
  let component: ProjectsOverviewComponent;
  let fixture: ComponentFixture<ProjectsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectsOverviewComponent],
      imports: [HttpClientModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
