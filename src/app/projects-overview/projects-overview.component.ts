import { Component, OnInit, Input } from "@angular/core";
import { TeamService } from "../team.service";
import { Project } from "../project";
import { EventBusService } from "../event-bus.service";

@Component({
  selector: "app-projects-overview",
  templateUrl: "./projects-overview.component.html",
  styleUrls: ["./projects-overview.component.css"]
})
export class ProjectsOverviewComponent implements OnInit {
  @Input() teamId: string;
  projects: Project[];

  constructor(
    private teamService: TeamService,
    private eventBusService: EventBusService
  ) {}

  ngOnInit() {
    this.eventBusService.messages.subscribe(message => {
      if (
        message.type === "CONNECTOR_CREATED" &&
        message.data === this.teamId
      ) {
        this.getProjects();
      }
    });
    this.getProjects();
  }

  getProjects() {
    this.teamService
      .getProjects(this.teamId)
      .subscribe(projects => (this.projects = projects));
  }
}
