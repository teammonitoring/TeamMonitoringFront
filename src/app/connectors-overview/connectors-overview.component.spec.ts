import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ConnectorsOverviewComponent } from "./connectors-overview.component";
import {
  MatInputModule,
  MatSelectModule,
  MatIconModule
} from "@angular/material";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe("ConnectorsOverviewComponent", () => {
  let component: ConnectorsOverviewComponent;
  let fixture: ComponentFixture<ConnectorsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectorsOverviewComponent],
      imports: [
        MatInputModule,
        MatSelectModule,
        MatIconModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectorsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
