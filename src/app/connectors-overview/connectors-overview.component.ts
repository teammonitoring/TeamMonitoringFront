import { Component, OnInit, Input } from "@angular/core";
import { ConnectorService } from "../connector.service";
import { Connector } from "../connector";
import { EventBusService } from "../event-bus.service";

@Component({
  selector: "app-connectors-overview",
  templateUrl: "./connectors-overview.component.html",
  styleUrls: ["./connectors-overview.component.css"]
})
export class ConnectorsOverviewComponent implements OnInit {
  @Input() teamId: string;
  connectors: Connector[];

  constructor(
    private connectorService: ConnectorService,
    private eventBusService: EventBusService
  ) {}

  ngOnInit() {
    this.eventBusService.messages.subscribe(message => {
      if (
        message.type === "CONNECTOR_CREATED" &&
        message.data === this.teamId
      ) {
        this.getConnectors();
      }
    });

    this.getConnectors();
  }

  getConnectors() {
    this.connectorService
      .getConnectorsByTeamId(this.teamId)
      .subscribe(connectors => (this.connectors = connectors));
  }

  delete(id: string) {
    this.connectorService
      .deleteConnector(id)
      .subscribe(() => this.getConnectors());
  }
}
