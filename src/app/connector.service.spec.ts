import { TestBed } from "@angular/core/testing";

import { ConnectorService } from "./connector.service";
import { HttpClientModule } from "@angular/common/http";

describe("ConnectorService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    })
  );

  it("should be created", () => {
    const service: ConnectorService = TestBed.get(ConnectorService);
    expect(service).toBeTruthy();
  });
});
