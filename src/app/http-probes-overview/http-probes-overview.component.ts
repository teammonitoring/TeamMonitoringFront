import { Component, Input, OnChanges } from "@angular/core";
import { Probe } from "../probe";

@Component({
  selector: "app-http-probes-overview",
  templateUrl: "./http-probes-overview.component.html",
  styleUrls: ["./http-probes-overview.component.css"]
})
export class HttpProbesOverviewComponent implements OnChanges {
  @Input() probes: Probe[];
  @Input() canDelete: boolean;
  reduced: Array<any>;

  constructor() {}
  ngOnChanges() {
    this.reduce();
  }

  reduce() {
    this.reduced = this.probes
      ? this.probes.reduce((acc, value) => {
          return (acc.some(item => item.name === value.tag)
            ? acc
            : [{ name: value.tag, probes: [] }, ...acc]
          ).map(group => {
            if (group.name === value.tag)
              return {
                name: value.tag,
                probes: [value, ...group.probes]
              };
            return group;
          });
        }, [])
      : undefined;
  }
}
