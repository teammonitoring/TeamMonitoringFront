import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { AppModule } from "../app.module";
import { HttpProbesOverviewComponent } from "./http-probes-overview.component";

describe("HttpProbesOverviewComponent", () => {
  let component: HttpProbesOverviewComponent;
  let fixture: ComponentFixture<HttpProbesOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpProbesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
