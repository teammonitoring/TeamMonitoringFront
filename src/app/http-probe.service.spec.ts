import { TestBed } from '@angular/core/testing';

import { HttpProbeService } from './http-probe.service';
import { HttpClientModule } from '@angular/common/http';

describe('HttpProbeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: HttpProbeService = TestBed.get(HttpProbeService);
    expect(service).toBeTruthy();
  });
});
