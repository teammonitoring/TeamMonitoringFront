import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppComponent } from "./app.component";
import { TeamsComponent } from "./teams/teams.component";
import { AppRoutingModule } from "./app-routing.module";
import { TeamEditionComponent } from "./team-edition/team-edition.component";
import { FormsModule } from "@angular/forms";
import { TeamOverviewComponent } from "./team-overview/team-overview.component";
import { HttpProbeCreationFormComponent } from "./http-probe-creation-form/http-probe-creation-form.component";
import { HttpProbeOverviewComponent } from "./http-probe-overview/http-probe-overview.component";
import { HttpProbesOverviewComponent } from "./http-probes-overview/http-probes-overview.component";
import { ConnectorCreationFormComponent } from "./connector-creation-form/connector-creation-form.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatSelectModule,
  MatInputModule,
  MatCardModule,
  MatTabsModule,
  MatGridListModule,
  MatButtonModule,
  MatIconModule,
  MatToolbarModule,
  MatDividerModule
} from "@angular/material";
import { TeamCreationFormComponent } from './team-creation-form/team-creation-form.component';
import { TeamCardComponent } from './team-card/team-card.component';
import { ConnectorsOverviewComponent } from './connectors-overview/connectors-overview.component';
import { ProjectsOverviewComponent } from './projects-overview/projects-overview.component';
import { MergeRequestsOverviewComponent } from './merge-requests-overview/merge-requests-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    TeamsComponent,
    TeamEditionComponent,
    TeamOverviewComponent,
    HttpProbeCreationFormComponent,
    HttpProbeOverviewComponent,
    HttpProbesOverviewComponent,
    ConnectorCreationFormComponent,
    TeamCreationFormComponent,
    TeamCardComponent,
    ConnectorsOverviewComponent,
    ProjectsOverviewComponent,
    MergeRequestsOverviewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    MatTabsModule,
    MatGridListModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatDividerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
