import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { AppModule } from "../app.module";
import { TeamEditionComponent } from "./team-edition.component";
import { FormsModule } from "@angular/forms";
import { HttpProbeCreationFormComponent } from "../http-probe-creation-form/http-probe-creation-form.component";
import { HttpProbeOverviewComponent } from "../http-probe-overview/http-probe-overview.component";
import { AppRoutingModule } from "../app-routing.module";
import { TeamOverviewComponent } from "../team-overview/team-overview.component";
import { TeamsComponent } from "../teams/teams.component";
import { HttpClientModule } from "@angular/common/http";

describe("TeamEditionComponent", () => {
  let component: TeamEditionComponent;
  let fixture: ComponentFixture<TeamEditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, AppRoutingModule, HttpClientModule, AppModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
