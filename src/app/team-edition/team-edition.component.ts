import { Component, OnInit, Input } from "@angular/core";
import { Team } from "../team";
import { TeamService } from "../team.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { HttpProbeService } from "../http-probe.service";
import { Probe } from "../probe";
import { EventBusService } from "../event-bus.service";

@Component({
  selector: "app-team-edition",
  templateUrl: "./team-edition.component.html",
  styleUrls: ["./team-edition.component.css"]
})
export class TeamEditionComponent implements OnInit {
  @Input() team: Team;
  probes: Probe[];
  navigated: boolean;

  constructor(
    private router: Router,
    private teamService: TeamService,
    private httpProbeService: HttpProbeService,
    private eventBusService: EventBusService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.eventBusService.messages.subscribe(message => {
      if (message.type === "TEAM_UPDATED" && this.team.id === message.data) {
        this.load(message.data);
      }
    });
    this.route.params.forEach((params: Params) => {
      if (params["id"] !== undefined) {
        this.navigated = true;
        const id = params["id"];
        this.load(id);
      } else {
        this.navigated = false;
        this.team = new Team();
      }
    });
  }

  load(id: string) {
    this.teamService.getTeam(id).subscribe(team => (this.team = team));
    this.httpProbeService.getProbesByTeamId(id).subscribe(probes => {
      this.probes = probes;
    });
  }

  save(): void {
    this.teamService
      .updateTeam(this.team)
      .subscribe(team => this.load(team.id));
  }

  goToOverview(id: string) {
    this.router.navigate(["/teams∕overview/", id]);
  }
}
