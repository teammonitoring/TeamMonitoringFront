import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { TeamCardComponent } from "./team-card.component";
import {
  MatCardModule,
  MatIconModule,
  MatDividerModule,
  MatInputModule,
  MatTabsModule,
  MatGridListModule,
  MatOptionModule,
  MatSelectModule
} from "@angular/material";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "../app-routing.module";
import { TeamOverviewComponent } from "../team-overview/team-overview.component";
import { TeamCreationFormComponent } from "../team-creation-form/team-creation-form.component";
import { TeamEditionComponent } from "../team-edition/team-edition.component";
import { TeamsComponent } from "../teams/teams.component";
import { HttpProbesOverviewComponent } from "../http-probes-overview/http-probes-overview.component";
import { HttpProbeOverviewComponent } from "../http-probe-overview/http-probe-overview.component";
import { FormsModule } from "@angular/forms";
import { HttpProbeCreationFormComponent } from "../http-probe-creation-form/http-probe-creation-form.component";
import { ConnectorCreationFormComponent } from "../connector-creation-form/connector-creation-form.component";
import { ConnectorsOverviewComponent } from "../connectors-overview/connectors-overview.component";
import { ProjectsOverviewComponent } from "../projects-overview/projects-overview.component";
import { MergeRequestsOverviewComponent } from "../merge-requests-overview/merge-requests-overview.component";

describe("TeamCardComponent", () => {
  let component: TeamCardComponent;
  let fixture: ComponentFixture<TeamCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TeamCardComponent,
        TeamOverviewComponent,
        TeamCreationFormComponent,
        TeamEditionComponent,
        TeamsComponent,
        HttpProbesOverviewComponent,
        HttpProbeOverviewComponent,
        HttpProbeCreationFormComponent,
        ConnectorCreationFormComponent,
        ConnectorsOverviewComponent,
        ProjectsOverviewComponent,
        MergeRequestsOverviewComponent
      ],
      imports: [
        MatCardModule,
        MatIconModule,
        MatInputModule,
        MatTabsModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        MatDividerModule,
        BrowserAnimationsModule,
        MatGridListModule,
        MatOptionModule,
        MatSelectModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
