import { Component, OnInit, Input } from "@angular/core";
import { Team } from "../team";
import { TeamService } from "../team.service";
import { EventBusService } from "../event-bus.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-team-card",
  templateUrl: "./team-card.component.html",
  styleUrls: ["./team-card.component.css"]
})
export class TeamCardComponent implements OnInit {
  @Input() team: Team;

  constructor(
    private router: Router,
    private teamService: TeamService,
    private eventBusService: EventBusService
  ) {}

  ngOnInit() {}

  deleteTeam(id: string) {
    this.teamService.deleteTeam(id).subscribe(() => {
      this.eventBusService.publish({
        type: "TEAM_DELETED",
        data: this.team.id
      });
    });
  }

  goToEdition(id: string) {
    this.router.navigate(["/teams/edit/", id]);
  }

  goToOverview(id: string) {
    this.router.navigate(["/teams∕overview/", id]);
  }
}
