import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { TeamService } from "../team.service";
import { Team } from "../team";
import { HttpProbeService } from "../http-probe.service";
import { Probe } from "../probe";
import { EventBusService } from "../event-bus.service";

@Component({
  selector: "app-team-overview",
  templateUrl: "./team-overview.component.html",
  styleUrls: ["./team-overview.component.css"]
})
export class TeamOverviewComponent implements OnInit {
  @Input() team: Team;
  probes: Probe[];

  constructor(
    private router: Router,
    private teamService: TeamService,
    private httpProbeService: HttpProbeService,
    private eventBusService: EventBusService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.eventBusService.messages.subscribe(message => {
      if (message.type === "TEAM_UPDATED" && this.team.id === message.data) {
        this.load(message.data);
      }
      if (message.type === "TEAM_DELETED" && this.team.id === message.data) {
        this.router.navigate(["/teams/"]);
      }
    });
    this.route.params.forEach((params: Params) => {
      if (params["id"] !== undefined) {
        const id = params["id"];
        this.load(id);
      } else {
        this.team = new Team();
      }
    });
  }

  load(id: string) {
    this.teamService.getTeam(id).subscribe(team => (this.team = team));
    this.httpProbeService
      .getProbesByTeamId(id)
      .subscribe(probes => (this.probes = probes));
  }
}
