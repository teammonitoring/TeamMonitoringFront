import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { AppModule } from "../app.module";
import { TeamOverviewComponent } from "./team-overview.component";
import { HttpClientModule } from "@angular/common/http";
import { HttpProbeOverviewComponent } from "../http-probe-overview/http-probe-overview.component";
import { AppRoutingModule } from "../app-routing.module";
import { TeamEditionComponent } from "../team-edition/team-edition.component";
import { TeamsComponent } from "../teams/teams.component";
import { FormsModule } from "@angular/forms";
import { HttpProbeCreationFormComponent } from "../http-probe-creation-form/http-probe-creation-form.component";

describe("TeamOverviewComponent", () => {
  let component: TeamOverviewComponent;
  let fixture: ComponentFixture<TeamOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, AppRoutingModule, FormsModule, AppModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
