export class Project {
  name: string;
  avatar: string;
  description: string;
  lastUpdateDate: string;
}
