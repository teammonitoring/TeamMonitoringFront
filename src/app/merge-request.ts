export class MergeRequest {
  title: string;
  description: string;
  state: string;
  createdAt: string;
}
