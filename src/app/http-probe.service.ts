import { Injectable } from "@angular/core";
import { Probe } from "./probe";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class HttpProbeService {
  private probesUrl: string = "/v1/probes";

  constructor(private httpClient: HttpClient) {}

  getProbes(): Observable<Probe[]> {
    return this.httpClient.get<Probe[]>(this.probesUrl);
  }

  getProbesByTeamId(teamId: string): Observable<Probe[]> {
    const url = `${this.probesUrl}?teamId=${teamId}`;
    return this.httpClient.get<Probe[]>(url);
  }

  getProbe(id: string): Observable<Probe> {
    const url = `${this.probesUrl}/${id}`;
    return this.httpClient.get<Probe>(url);
  }

  createProbe(probe: Probe): Observable<Probe> {
    return this.httpClient.post<Probe>(this.probesUrl, probe);
  }

  updateProbe(probe: Probe): Observable<Probe> {
    const url = `${this.probesUrl}/${probe.id}`;
    return this.httpClient.put<Probe>(url, probe);
  }

  deleteProbe(id: string): Observable<any> {
    const url = `${this.probesUrl}/${id}`;
    return this.httpClient.delete<any>(url);
  }
}
