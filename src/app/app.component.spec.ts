import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { TeamOverviewComponent } from "./team-overview/team-overview.component";
import { TeamEditionComponent } from "./team-edition/team-edition.component";
import { TeamsComponent } from "./teams/teams.component";
import { HttpProbeOverviewComponent } from "./http-probe-overview/http-probe-overview.component";
import { FormsModule } from "@angular/forms";
import { HttpProbeCreationFormComponent } from "./http-probe-creation-form/http-probe-creation-form.component";
import { AppModule } from "./app.module";

describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppRoutingModule, FormsModule, AppModule]
    }).compileComponents();
  }));

  it("should create the app", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
