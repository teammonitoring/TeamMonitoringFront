import { Injectable } from "@angular/core";
import { Team } from "./team";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { AVATAR } from "./default-avatar";
import { Project } from "./project";
import { MergeRequest } from "./merge-request";

@Injectable({
  providedIn: "root"
})
export class TeamService {
  private teamsUrl: string = "/v1/teams";

  constructor(private httpClient: HttpClient) {}

  getTeams(): Observable<Team[]> {
    return this.httpClient.get<Team[]>(this.teamsUrl);
  }

  getTeam(id: string): Observable<Team> {
    const url = `${this.teamsUrl}/${id}`;
    return this.httpClient.get<Team>(url);
  }

  createTeam(team: Team): Observable<Team> {
    if (!team.avatar) {
      team.avatar = AVATAR;
    }
    return this.httpClient.post<Team>(this.teamsUrl, team);
  }

  updateTeam(team: Team): Observable<Team> {
    const url = `${this.teamsUrl}/${team.id}`;
    return this.httpClient.put<Team>(url, team);
  }

  deleteTeam(id: string): Observable<any> {
    const url = `${this.teamsUrl}/${id}`;
    return this.httpClient.delete<any>(url);
  }

  getProjects(id: string): Observable<Project[]> {
    const url = `${this.teamsUrl}/${id}/projects`;
    return this.httpClient.get<Project[]>(url);
  }

  getMergeRequests(id: string): Observable<MergeRequest[]> {
    const url = `${this.teamsUrl}/${id}/merge-requests`;
    return this.httpClient.get<MergeRequest[]>(url);
  }
}
