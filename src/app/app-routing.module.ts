import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TeamsComponent } from "./teams/teams.component";
import { TeamEditionComponent } from "./team-edition/team-edition.component";
import { TeamOverviewComponent } from './team-overview/team-overview.component';

const routes: Routes = [
  { path: "teams∕overview/:id", component: TeamOverviewComponent },
  { path: "teams/edit/:id", component: TeamEditionComponent },
  { path: "", component: TeamsComponent },
  { path: "teams", component: TeamsComponent },
];

@NgModule({ imports: [RouterModule.forRoot(routes)], exports: [RouterModule] })
export class AppRoutingModule { }
